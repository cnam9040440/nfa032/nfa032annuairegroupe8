# ANNUAIRE DU GROUPE 8

Projet commun CNAM NFA032 --- Année 2023-2024

## Installation

ainsi que vous pourrez l'apprécier notre application est initialisée à son démarrage. Des fichiers Comptes et Annuaire sont générés et remplis avec des exemples.

## Usage

Un compte Admin est à votre disposition avec les identifiants suivants :
Mail : admin@cnam.fr / mot de passe : admin

## Commentaires sur la réalisation

Nous avons essayé de respecter votre demande et vous constaterez que la console bénéficie de coloris donnant une expérience esthétique à l’utilisateur.
Les fonctions principales ont été codées telles que vous les avez souhaitées et nous avons mis en place quelques contrôles simples de cohérence pour faire en sorte de rendre notre application robuste et fiable, parée à gérer des erreurs. Nous vous proposons de la mettre à l’épreuve.
Nous avons découpé et organisé notre code de manière à le rendre lisible et maintenable.

## Retour d'expérience

Nous tenons à vous signifier que ce projet aura mis à l’épreuve nos compétences en programmation et sur le plan humain, et aura fait grandir notre capacité d’adaptation et d’organisation.
Il s’agit pour nous d’une première expérience en Java. Nous avons beaucoup apprécié travailler ensemble sur ce projet que vous nous avez confié et espérons que notre travail le reflète.
Nous vous remercions et vous souhaitons une bonne expérience !

## Contribution

Ségolène MARIS,
Samir CHELGHOUM,
Olivier PIGNOT,
Sébastien LALLEMAND,
Antoine GUEYDON
