package travaildegroupe.annuairegroupe8;

import java.io.*;
import java.util.Scanner;

/**
 * classe gérant le fichier des comptes
 */
public class Comptes {

    /**
     * ajout d'un compte dans le fichier Comptes
     * @param fichierCompte
     * @param role
     */
    public void ajouterCompte (File fichierCompte, Role role, String email) throws IOException {

        Scanner sc = new Scanner(System.in);

        System.out.println("Veuillez créer un mot de passe pour ce compte");
        String motDePasse = sc.nextLine();
        while(motDePasse.isEmpty()){
            System.out.println("Vous n'avez pas entrer de mot de passe, veuillez créer un mot de passe pour ce compte");
            motDePasse = sc.nextLine();
        }


        if(rechercheCompte(fichierCompte, email) == null) {

            FileWriter fileWriter = new FileWriter(fichierCompte, true);
            BufferedWriter outputStream = new BufferedWriter(fileWriter);


             Compte compte = new Compte(email, motDePasse, role);
             String newCompte = compte.toString(compte);
             outputStream.write(newCompte);

            outputStream.newLine();
            outputStream.flush();
            fileWriter.close();
            outputStream.close();

            System.out.println(ConsoleColors.GREEN_BOLD + "Le compte avec pour email " + email + " a été crée" + ConsoleColors.RESET);
        } else {
            System.out.println(ConsoleColors.RED_BOLD + "Un compte correspondant à ce email existe déjà"+ ConsoleColors.RESET);
        }
    }


    /**
     * recherche si un compte correspondant à l'email trouvé existe dans le fichier
     * @param fichierCompte
     * @param email
     * @return Compte
     * @throws IOException
     */
    public Compte rechercheCompte(File fichierCompte, String email) throws IOException {

        FileReader fileReader = new FileReader(fichierCompte);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line = bufferedReader.readLine();
        while (line != null){
            String emailPossible = line.split(";")[0];
            if(email.equals(emailPossible)){
                String motDePasse = line.split(";")[1];
                String stringRole = line.split(";")[2];
                Compte compteFound = new Compte(email, motDePasse, Role.valueOf(stringRole));

                fileReader.close();
                bufferedReader.close();
                return compteFound;
            }

            line = bufferedReader.readLine();
        }
        fileReader.close();
        bufferedReader.close();
        return null;
    }
}
