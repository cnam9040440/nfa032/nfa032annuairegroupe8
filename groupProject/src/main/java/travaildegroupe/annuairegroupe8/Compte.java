package travaildegroupe.annuairegroupe8;

public class Compte {

    private String email;
    private String password;
    private Role role;


    /**
     * constructeur de l'objet Compte
     * @param email
     * @param password
     * @param role
     */
    public Compte(String email, String password, Role role) {
        this.email = email;
        this.password = password;
        this.role = role;
    }

    // Getters and Setters

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String toString(Compte compte){

        return compte.getEmail() + ";" + compte.getPassword() + ";" + compte.getRole().toString();

    }
}
