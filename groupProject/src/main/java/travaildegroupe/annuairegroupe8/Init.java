package travaildegroupe.annuairegroupe8;

import java.io.*;

public class Init {
    private static boolean fileCompte;
    private static boolean fileAnnuaire;
    private static boolean folderTMP;
    private static boolean createAdmin;
    private static boolean validationInit;

    private static final String RESOURCES_PATH = "groupProject/src/test/resources/";
    public static final String fichierCompte = RESOURCES_PATH + "Comptes.csv";
    public static final String fichierAnnuaire = RESOURCES_PATH + "Annuaire.csv";
    
    

    // Partie Get et Setter
    public static boolean isValidationInit() {
        return validationInit;
    }
    public static void setValidationInit(boolean validationInit) {
        Init.validationInit = validationInit;
    }
    public static boolean isCreateAdmin() {
        return createAdmin;
    }
    public static void setCreateAdmin(boolean createAdmin) {
        Init.createAdmin = createAdmin;
    }
    public static boolean isFolderTMP() {
        return folderTMP;
    }
    public static void setFolderTMP(boolean folderTMP) {
        Init.folderTMP = folderTMP;
    }
    public static boolean isFileAnnuaire() {
        return fileAnnuaire;
    }

    public static boolean isFileCompte() {
        return fileCompte;
    }

    public static void setFileAnnuaire(boolean fileAnnuaire) {
        Init.fileAnnuaire = fileAnnuaire;
    }

    static void setFileCompte(boolean fileCompte) {
        Init.fileCompte = fileCompte;
    }

    // Les fichiers Comptes et Annuaire sont créés sur le Pc de l'utilisateurs, en dehors du projet, car ce sont des fichiers dynamiques
    public static void Init()
    {
        setValidationInit(true); // Initialisation de la validation init
        VerifDossierTmp();
        VerifFichierCompte();
        VerifFichierAnnuaire();
    }
    public static void VerifDossierTmp()
    {
        // Vérification de l'existance du dossier c:\tmp
        String directoryPath = RESOURCES_PATH; // Remplacez par le chemin souhaité

        File directory = new File(directoryPath);
        if (!directory.exists()) {
            boolean isCreated = directory.mkdirs();
            if (isCreated) {
                setFolderTMP(true);
                System.out.println("Dossier tmp créé avec succès.");
            } else {
                setFolderTMP(false);
                setValidationInit(false);
                System.out.println("Échec de la création du dossier tmp.");
            }
        } else {
            setFolderTMP(true);
            System.out.println("Le dossier tmp existe déjà.");
        }

    }
    // Vérification de la présente du fichier des comptes
    public static void VerifFichierCompte()
    {
        if (isFolderTMP())
        {
            String filePath = fichierCompte; // Remplacez par le chemin souhaité

            File csvFile = new File(filePath);
            if (!csvFile.exists()) {
                try {
                    csvFile.createNewFile();

                    // Optionnel: Écrire des en-têtes ou des données initiales dans le fichier
                    FileWriter writer = new FileWriter(csvFile);
                    writer.append("email;mdp;rôle\n"); // Exemple d'en-têtes
                    // Ajouter plus de données si nécessaire
                    //On ajoute les comptes users
                    writer.append("alice.durand@exemple.com;password;user\n");
                    writer.append("bruno.leroy@exemple.com;password;user\n");
                    writer.append("clara.moreau@exemple.com;password;user\n");
                    writer.append("denis.simon@exemple.com;password;user\n");
                    writer.append("eva.laurent@exemple.com;password;user\n");
                    writer.append("francois.lefebvre@exemple.com;password;user\n");
                    writer.append("giselle.michel@exemple.com;password;user\n");
                    writer.append("hugo.garcia@exemple.com;password;user\n");
                    writer.append("irene.david@exemple.com;password;user\n");
                    writer.append("joel.bertrand@exemple.com;password;user\n");

                    writer.flush();
                    writer.close();
                    System.out.println("Le fichier comptes a bien été créé.");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            setFileCompte(true);
            System.out.println("Le fichier est présent.");
            VerifComteAdmin();
        }
        else
        {
            setFileCompte(false);
            setValidationInit(false);
        }


    }
    // Vérification de la présente du fichier d'annuaire
    public static void VerifFichierAnnuaire()
    {
        if (isFolderTMP())
        {
            String filePath = fichierAnnuaire; // Remplacez par le chemin souhaité

            File csvFile = new File(filePath);
            if (!csvFile.exists()) {
                try {
                    csvFile.createNewFile();

                    // Optionnel: Écrire des en-têtes ou des données initiales dans le fichier
                    FileWriter writer = new FileWriter(csvFile);
                    writer.append("nom;prenom;email;adressePostale;dateNaissance;profil;dateAjout;dateMaj\n");
                    System.out.println("Le fichier annuaire a bien été créé.");

                    // On remplie le fichier avec des données fictives
                    System.out.println("Création des comptes utilisateur.");
                    writer.append("Durand;Alice;alice.durand@exemple.com;123 Rue A;1990-01-01;Auditeurs;2023-01-01;2023-01-01\n");
                    writer.append("Leroy;Bruno;bruno.leroy@exemple.com;456 Rue B;1990-02-02;Enseignants;2023-01-02;2023-01-02\n");
                    writer.append("Moreau;Clara;clara.moreau@exemple.com;789 Rue C;1990-03-03;Enseignants;2023-01-03;2023-01-03\n");
                    writer.append("Simon;Denis;denis.simon@exemple.com;101 Rue D;1990-04-04;Direction;2023-01-04;2023-01-04\n");
                    writer.append("Laurent;Eva;eva.laurent@exemple.com;121 Rue E;1990-05-05;Auditeurs;2023-01-05;2023-01-05\n");
                    writer.append("Lefebvre;François;francois.lefebvre@exemple.com;141 Rue F;1990-06-06;Enseignants;2023-01-06;2023-01-06\n");
                    writer.append("Michel;Giselle;giselle.michel@exemple.com;161 Rue G;1990-07-07;Auditeurs;2023-01-07;2023-01-07\n");
                    writer.append("Garcia;Hugo;hugo.garcia@exemple.com;181 Rue H;1990-08-08;Enseignants;2023-01-08;2023-01-08\n");
                    writer.append("David;Irene;irene.david@exemple.com;201 Rue I;1990-09-09;Direction;2023-01-09;2023-01-09\n");
                    writer.append("Bertrand;Joel;joel.bertrand@exemple.com;221 Rue J;1990-10-10;Auditeurs;2023-01-10;2023-01-10\n");

                    writer.flush();
                    writer.close();
                    setFileAnnuaire(true);
                } catch (IOException e) {
                    e.printStackTrace();
                    setValidationInit(false);
                }
            }
            else
            {
                setFileAnnuaire(true);
                System.out.println("Le fichier annuaire est présent.");
            }

        }
        else
        {
            setFileAnnuaire(false);
            setValidationInit(false);
        }
    }

    // Vérification de la présente du compte admin dans le fichier des comptes
    public static void VerifComteAdmin()
    {
        if (isFileCompte())
        {
            String filePath = fichierCompte;
            String lineToCheck = "admin@cnam.fr;admin;admin";
            boolean lineExists = false;

            // Vérifier si la ligne existe
            try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.equals(lineToCheck)) {
                        lineExists = true;
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Écrire la ligne si elle n'existe pas
            if (!lineExists) {
                try (FileWriter fw = new FileWriter(filePath, true)) { // Mode append activé
                    fw.write(lineToCheck + "\n");
                    setCreateAdmin(true);
                    System.out.println("Le compte admin a été ajouté au fichier des comptes.");
                } catch (IOException e) {
                    setCreateAdmin(false);
                    setValidationInit(false);
                    e.printStackTrace();
                }
            } else {
                setCreateAdmin(true);
                System.out.println("Le compte admin est déjà présent dans le fichier.");
            }
        }
    }

    // Création du compte admin
    public static void CreationAdmin()
    {
        try
        {
            String filePath = fichierCompte;
            FileWriter writer = new FileWriter(filePath);
            writer.append("admin@cnam.fr;admin;admin\n");
            writer.flush();
            writer.close();
            setCreateAdmin(true);
        }
        catch (IOException e)
        {
            setCreateAdmin(false);
        }

    }



}
