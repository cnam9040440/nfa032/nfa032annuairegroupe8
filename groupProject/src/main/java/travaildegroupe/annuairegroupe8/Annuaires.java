package travaildegroupe.annuairegroupe8;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Annuaires {

    private static String creationDate;


    /**
     * Cette méthode permet l'ajout d'une personne à l'annuaire
     * @param fichierAnnuaire
     * @param email
     * @throws IOException
     */
    public void ajoutAnnuaire(File fichierAnnuaire, String email, Boolean ajout) throws IOException {

            Scanner sc = new Scanner(System.in);

            System.out.println("Veuillez indiquer le nom de la personne:");
            String nom = sc.nextLine();
            while(nom.isEmpty()){
                System.out.println("Vous n'avez pas indiqué de nom, veuillez renseigner le nom:");
                nom = sc.nextLine();
            }


            System.out.println("Veuillez indiquer le prénom:");
            String prenom = sc.nextLine();
            while(prenom.isEmpty()){
                System.out.println("Vous n'avez pas indiqué de prénom, veuillez renseigner le prénom:");
                prenom = sc.nextLine();
            }



            System.out.println("Veuillez indiquer l'adresse postale:");
            String adresse = sc.nextLine();
            while(adresse.isEmpty()){
                System.out.println("Vous n'avez pas indiqué d'adresse postale, veuillez renseigner l'adresse postale:");
                adresse = sc.nextLine();
            }


            int dateOk = 0;
            Date dateNaissance = null;
            while(dateOk == 0) {
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    System.out.println("Veuillez indiquer la date de naissance (yyyy-MM-dd):");
                    String dateNaissanceString = sc.nextLine();
                    String regx = "^\\d{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";
                    Pattern pattern = Pattern.compile(regx);
                    Matcher matcher = pattern.matcher(dateNaissanceString);
                    if(matcher.matches()) {
                        dateNaissance = formatter.parse(dateNaissanceString);
                        dateOk = 1;
                    }
                    else System.out.println(ConsoleColors.RED_BOLD + "Date de naissance invalide" + ConsoleColors.RESET);

                } catch (ParseException e) {
                    System.out.println(ConsoleColors.RED_BOLD + "Date de naissance invalide" + ConsoleColors.RESET);
                    dateOk = 0;

                }
            }

            int roleOk = 0;
            String profilString ="";
            while (roleOk == 0) {
                System.out.println("Veuillez indiquer le profil parmis les choix suivant: Auditeurs, Enseignants et Direction");
                profilString = sc.nextLine();
                if(Objects.equals(profilString, Profil.Auditeurs.name()) || Objects.equals(profilString, Profil.Enseignants.name()) || Objects.equals(profilString, Profil.Direction.name())){
                //if(Objects.equals(profilString, "Auditeurs") || Objects.equals(profilString, "Enseignants") || Objects.equals(profilString, "Direction")){

                    roleOk = 1;
                    break;
                }
                else {
                    System.out.println(ConsoleColors.RED_BOLD + "profil invalide" + ConsoleColors.RESET);
                    roleOk = 0;
                }


            }



            Profil profil = Profil.valueOf(profilString);

            Date date = new Date();

            FileWriter fileWriter = new FileWriter(fichierAnnuaire, true);
            BufferedWriter outputStream = new BufferedWriter(fileWriter);

            Annuaire annuaire;

            if (ajout) {
                annuaire = new Annuaire(nom, prenom, dateNaissance, adresse, email, profil, date, date);
            } else {

                annuaire = new Annuaire(nom, prenom, dateNaissance, adresse, email, profil, Annuaires.creationDate, date);
            }
            String newAnnuaire = annuaire.toString(annuaire, ajout);
            outputStream.write(newAnnuaire);

            if (ajout) {
                System.out.println(ConsoleColors.GREEN_BOLD + "Un ajout a été effectué dans l'annuaire" + ConsoleColors.RESET);
            } else {
                System.out.println(ConsoleColors.GREEN_BOLD + "Une modification a été effectué dans l'annuaire" + ConsoleColors.RESET);
            }

            outputStream.newLine();
            outputStream.flush();
            fileWriter.close();
            outputStream.close();
        }


    /**
     * Cette méthode permet de modifier une ligne. Elle supprime la ligne correspondant à l'email de l'utilisateur et la remplace par
     * la nouvelle ligne saisie en appellant la methode ajoutAnnuaire
     * @param mail
     * @throws IOException
     */
    public void modifAnnuaire(File fichierAnnuaire, String mail) throws IOException {

        Boolean supprimer = supprimerAnnuaire(fichierAnnuaire, mail);
        if(supprimer){
            ajoutAnnuaire(fichierAnnuaire, mail, false);
        }

    }

    /**
     *
     * Cette méthode supprime une ligne du fichier Annuaire. Elle filtre d'abord toutes les lignes qui ne doivent pas être supprimées,
     * et les stocke dans une collection, elle réinitialise le fichier annuaire pour y recopier toutes les lignes issues du filtrage.
     * Seule la ligne à supprimer ne sera pas recopiée.
     * @param mail
     * @throws IOException
     */
    public Boolean supprimerAnnuaire(File fichierAnnuaire, String mail) throws IOException {


        FileReader fileReader = new FileReader(fichierAnnuaire);
        BufferedReader inputStream = new BufferedReader(fileReader);


        List<String> liste = inputStream.lines()
                        .filter(line -> !line.split(";")[2].equals(mail))
                        .toList();


        fileReader.close();
        inputStream.close();

        FileReader fileReader2 = new FileReader(fichierAnnuaire);
        BufferedReader inputStream2 = new BufferedReader(fileReader2);

        String ligne = inputStream2.readLine();
        String dateCreation = "";
        while (ligne != null){
            List<String> list = Arrays.stream(ligne.split(";")).toList();
            if(list.get(2).equals(mail)){
                dateCreation= list.get(6);
                break;
            }
            ligne = inputStream2.readLine();
        }
        if(dateCreation.isEmpty()){
            System.out.println(ConsoleColors.RED_BOLD + "Ce mail ne correspond à personne dans l'annuaire"+ ConsoleColors.RESET);
            return false;
        }

        fileReader2.close();
        inputStream2.close();


      Annuaires.creationDate = dateCreation;

        FileWriter fileWriter = new FileWriter(fichierAnnuaire);
        BufferedWriter outputStream = new BufferedWriter(fileWriter);

        liste.forEach(line -> {
            try {
                if(liste.indexOf(line) == liste.size()){
                    outputStream.write(line);
                }
                else {
                    outputStream.write(line);
                    outputStream.newLine();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });


            outputStream.flush();
            fileWriter.close();
            outputStream.close();
            fileReader.close();
            inputStream.close();

            return true;
    }

    /**
     * Méthode permettant de rechercher par nom la ou les lignes de l'annuaire correspondant au String passé en paramètre.
     * cette méthode appelle la methode rechercheAnnuaire(String info, int type) en lui passant le String nom et le int 0 correspondant à l'indice du split
     * utilisé par la méthode rechercheAnnuaire(String info, int type) pour sélectionner le champs de la ligne observée à comparer avec le String nom.
     * @param nom
     * @return List<String>
     * @throws FileNotFoundException
     */
    public List<String> rechercheAnnuaireNom(File fichierAnnuaire, String nom) throws FileNotFoundException { return rechercheAnnuaire(fichierAnnuaire, nom, 0);}
    /**
     * Méthode permettant de rechercher par mail la ou les lignes de l'annuaire correspondant au String passé en paramètre.
     * cette méthode appelle la methode rechercheAnnuaire(String info, int type) en lui passant le String mail et le int 0 correspondant à l'indice du split
     * utilisé par la méthode rechercheAnnuaire(String info, int type) pour sélectionner le champs de la ligne observée à comparer avec le String mail.
     * @param mail
     * @return List<String>
     * @throws FileNotFoundException
     */
    public List<String> rechercheAnnuaireMail(File fichierAnnuaire, String mail) throws FileNotFoundException { return  rechercheAnnuaire(fichierAnnuaire, mail , 2);}
    /**
     * Méthode permettant de rechercher par profil la ou les lignes de l'annuaire correspondant au String passé en paramètre.
     * cette méthode appelle la methode rechercheAnnuaire(String info, int type) en lui passant le String profil et le int 0 correspondant à l'indice du split
     * utilisé par la méthode rechercheAnnuaire(String info, int type) pour sélectionner le champs de la ligne observée à comparer avec le String profil.
     * @param profil
     * @return List<String>
     * @throws FileNotFoundException
     */
    public List<String> rechercheAnnuaireProfil(File fichierAnnuaire, String profil) throws FileNotFoundException {return  rechercheAnnuaire(fichierAnnuaire, profil , 5);}

    /**
     * Methode permettant de renvoyer une liste de 10 occurences maximum correspondant aux lignes  du fichier annuaire.
     * le type d'information recherchée est passée en parametre int type, qui correspond à l'indice du split sur la ligne observée.
     * Si la case d'indice type du split de la ligne est égal au string info, alors la ligne est stockée dans la liste liste.
     * Si il n'ya  plus aucune ligne à lire sur le fichier ou que 10 lignes ont été stockées, la méthode renvoie la liste liste.
     * @param info
     * @param type
     * @return
     * @throws FileNotFoundException
     */
    public List<String> rechercheAnnuaire(File fichierAnnuaire, String info, int type) throws FileNotFoundException {

        String line;
        List<String> liste = new ArrayList<>();

        Scanner scan = new Scanner(fichierAnnuaire);

        while (scan.hasNextLine() && (liste.size() < 10)) {

            line = scan.nextLine();
            if(line.split(";")[type].equalsIgnoreCase(info)){
                liste.add(line);
            }
        }
        if(liste.isEmpty()) {
            System.out.println(ConsoleColors.RED_BOLD + "Il n'existe personne correspondant à cette recherche" + ConsoleColors.RESET);
        }
        return  liste;
    }

    /**
     * Cette methode affiche une liste en console. Elle est utilse pour affihcer le résultat des fonctions de recherches dans l'annuaire.
     * @param liste
     */
    public void afficherRechercheAnnuaire(List<String> liste){

        for(String l : liste){
            System.out.println(l);
        }
    }


}
