package travaildegroupe.annuairegroupe8;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        // Initialisation du projet
        System.out.println("=======================================================================================");
        System.out.println("                                initialisation du programme                            ");
        System.out.println("=======================================================================================");
        Init.Init();
        if (Init.isValidationInit()) {
            System.out.println("=======================================================================================");
            System.out.println("                    Le programme est initialisé correctement                           ");
            System.out.println("=======================================================================================");

              File fichierCompte = new File(Init.fichierCompte);
              File fichierAnnuaire = new File(Init.fichierAnnuaire);

            Menu menu = new Menu();
            menu.afficheMenuPrincipal(fichierCompte, fichierAnnuaire);
        }
        else
        {
            System.out.println("=======================================================================================");
            System.out.println("Le programme ne peut pas aller car une erreur fatale d'initialisation a été rencontré !");
            System.out.println("=======================================================================================");
        }
    }
}