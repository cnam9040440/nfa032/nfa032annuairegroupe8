package travaildegroupe.annuairegroupe8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Menu {

    private int choixPrincipal;
    private char choixSecondaire;
    private Scanner scan = new Scanner(System.in);

    /**
     * permet d'afficher le menu principal
     * @param fichierCompte
     * @param fichierAnnuaire
     */
    void afficheMenuPrincipal(File fichierCompte, File fichierAnnuaire) {

        do {
            System.out.println("\t\t" +
                    ConsoleColors.WHITE_UNDERLINED + "\n\nBienvenue dans l'annuaire NFA032 du GROUPE 8\n" + ConsoleColors.RESET);
            System.out.println(ConsoleColors.WHITE_BOLD + "Administrateur :" + ConsoleColors.RESET);
            System.out.println("1. Ajouter une personne");
            System.out.println("\t" + ConsoleColors.YELLOW + "A. Ajouter un Admin");
            System.out.println("\tB. Ajouter un Particulier\n" + ConsoleColors.RESET);

            System.out.println(ConsoleColors.WHITE_BOLD + "Particulier :" + ConsoleColors.RESET);
            System.out.println("2. Rechercher un ou des particuliers");
            System.out.println("\t" + ConsoleColors.YELLOW + "A. Par nom");
            System.out.println("\tB. Par email");
            System.out.println("\tC. Par profil\n" + ConsoleColors.RESET);
            System.out.println("3. Modifier mes informations personnelles\n");


            do {
                try {
                    System.out.print("Faites votre choix: ");
                    choixPrincipal = scan.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("Choix incorrect");
                    scan.next();
                    choixPrincipal = 0;
                }
            } while (choixPrincipal <= 0 || choixPrincipal >= 4);

            switch (choixPrincipal) {
                case 1:
                    Authentification autAdmin = new Authentification();
                    if (autAdmin.login(fichierCompte) == Role.admin) {
                        System.out.println(ConsoleColors.GREEN_BOLD + "Utilisateur connecté" + ConsoleColors.RESET);

                        afficheMenuAdmin(fichierCompte, fichierAnnuaire);
                    } else {
                        System.out.println(ConsoleColors.RED_BOLD + "Utilisateur incorrect" + ConsoleColors.RESET);
                    }
                    break;
                case 2:
                    afficheMenuParticulier(fichierCompte, fichierAnnuaire);
                    break;
                case 3:
                    Authentification autUser = new Authentification();
                    Role role = autUser.login(fichierCompte);
                    if (role != null) {
                        System.out.println(ConsoleColors.GREEN_BOLD + "Utilisateur connecté" + ConsoleColors.RESET);
                        String email = "";
                        Annuaires annuaires = new Annuaires();
                        switch (role){
                            case Role.admin:
                                System.out.println("Veuillez indiquer le mail de la personne a modifier:");
                                scan.nextLine();
                                email = scan.nextLine();
                                break;
                            case Role.user:
                                email = autUser.getMail();
                        }
                        try {
                            annuaires.modifAnnuaire(fichierAnnuaire, email);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                     else {
                        System.out.println(ConsoleColors.RED_BOLD + "Utilisateur incorrect" + ConsoleColors.RESET);
                    }
                    break;

            }

        } while (choixPrincipal != 4);

    }


    /**
     * permet d'afficher le menu administrateur
     * @param fichierCompte
     * @param fichierAnnuaire
     */
    void afficheMenuAdmin(File fichierCompte, File fichierAnnuaire) {
        do {
            System.out.println("\n---------------------------------------");
            System.out.println("Voulez-vous : ");
            System.out.println("\tA. Ajouter un Admin");
            System.out.println("\tB. Ajouter un Particulier");
            System.out.println("\tQ. Revenir au menu précédent");
            System.out.print("\nVotre choix : ");
            choixSecondaire = scan.next().charAt(0);
            scan.nextLine();
            String email = "";

            switch (choixSecondaire) {
                case 'A':
                    email = renseignerMail();
                    ajouterAdmin(fichierCompte, email);
                    break;
                case 'B':
                    email = renseignerMail();
                    ajouterParticulier(fichierCompte, fichierAnnuaire, email);
                    break;
                case 'Q':
                    retournerMenuPrecedent();
                    break;
                default:
                    afficheMenuAdmin(fichierCompte, fichierAnnuaire/*, email*/);
                    break;
            }
        } while (choixSecondaire != 'Q');
    }

    /**
     * méthode permettant de renseigner un mail
     * @return String
     */
    public String renseignerMail(){
        String regx = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
        Pattern pattern = Pattern.compile(regx);
        int emailok = 0;
        String email = "";
        while(emailok == 0) {
            System.out.println("Veuillez indiquer le mail de la personne a ajouter:");
            email = scan.nextLine();
            Matcher matcher = pattern.matcher(email);
            emailok = 1;
            if (!matcher.matches()){
                System.out.println(ConsoleColors.RED_BOLD + "email invalide" + ConsoleColors.RESET);
                emailok = 0;
            }

        }return email;
    }

    /**
     * permet d'afficher le menu utilisateur particulier
     * @param fichierComptes
     * @param fichierAnnuaire
     */
    void afficheMenuParticulier(File fichierComptes, File fichierAnnuaire) {
        Annuaires annuaires = new Annuaires();
        do {
            System.out.println("\n---------------------------------------");
            System.out.println("Voulez-vous : ");
            System.out.println("\tA. Par nom");
            System.out.println("\tB. Par email");
            System.out.println("\tC. Par profil");
            System.out.println("\tQ. Revenir en arrière\n");
            System.out.print("Votre choix : ");
            choixSecondaire = scan.next().charAt(0);
            switch (choixSecondaire) {
                case 'A':
                    System.out.println("Veuillez indiquer le nom de la personne que vous souhaitez rechercher:");
                    scan.nextLine();
                    String nom = scan.nextLine();
                    try {
                        List<String> listPersonnes = annuaires.rechercheAnnuaireNom(fichierAnnuaire, nom);
                        annuaires.afficherRechercheAnnuaire(listPersonnes);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                    break;
                case 'B':
                    System.out.println("Veuillez indiquer le mail de la personne que vous souhaitez rechercher:");
                    scan.nextLine();
                    String email = scan.nextLine();
                    try {
                        List<String> listPersonnes = annuaires.rechercheAnnuaireMail(fichierAnnuaire,email);
                        annuaires.afficherRechercheAnnuaire(listPersonnes);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                    break;
                case 'C':
                    System.out.println("Veuillez indiquer le profil de la personne que vous souhaitez rechercher parmis les suivants: Auditeurs, Enseignants et Direction");
                    scan.nextLine();
                    String profil = scan.nextLine();
                    try {
                        List<String> listPersonnes = annuaires.rechercheAnnuaireProfil(fichierAnnuaire, profil);
                        annuaires.afficherRechercheAnnuaire(listPersonnes);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                    break;
                case 'Q':
                    retournerMenuPrecedent();
                    break;
                default:
                    afficheMenuPrincipal(fichierComptes, fichierAnnuaire);
                    break;
            }
        } while (choixSecondaire != 'Q');

    }


    /**
     * méthode permettant l'ajour d'un administrateur
     * @param fichierCompte
     * @param email
     */
    void ajouterAdmin(File fichierCompte, String email) {
        Comptes comptes = new Comptes();
        Compte compte;

        try {
            comptes.ajouterCompte(fichierCompte, Role.admin, email);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

    }

    /**
     * méthode permettant l'ajout d'un utilisateur particulier
     * @param fichierCompte
     * @param fichierAnnuaire
     * @param email
     */
    void ajouterParticulier(File fichierCompte, File fichierAnnuaire, String email) {
        Comptes comptes = new Comptes();
        Compte compte;
        Annuaires annuaires = new Annuaires();
        try {
            compte = comptes.rechercheCompte(fichierCompte, email);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (compte == null){
            try {
                annuaires.ajoutAnnuaire(fichierAnnuaire, email, true);
                comptes.ajouterCompte(fichierCompte, Role.user, email);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
        else{
            System.out.println(ConsoleColors.RED_BOLD + "Un enregistrement correspondant à ce email existe déjà dans l'annuaire"+ ConsoleColors.RESET);
        }
    }


    /**
     * permet le retour au menu précédant
     */
    void retournerMenuPrecedent() {
        System.out.println("retour\n");
    }

}
