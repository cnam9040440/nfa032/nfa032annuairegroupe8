package travaildegroupe.annuairegroupe8;

import java.util.Date;

public class Annuaire {

    private String nom;
    private String prenom;
    private Date dateDeNaissance;
    private String adressePostale;
    private String email;

    private Profil profil;

    private Date dateAjout;
    private Date dateMaj;

    private String dateCreation;



    /**
     * constructeur de l'object Annuaire dans le cas d'un ajout
     * @param nom
     * @param prenom
     * @param dateDeNaissance
     * @param adressePostale
     * @param email
     * @param profil
     * @param dateAjout
     * @param dateMaj
     */
    public Annuaire(String nom, String prenom, Date dateDeNaissance, String adressePostale, String email, Profil profil, Date dateAjout, Date dateMaj) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateDeNaissance = dateDeNaissance;
        this.adressePostale = adressePostale;
        this.email = email;
        this.profil = profil;
        this.dateAjout = dateAjout;
        this.dateMaj = dateMaj;
    }

    /**
     * constructeur de l'objet Annuaire dans le cas d'une modification
     * @param nom
     * @param prenom
     * @param dateDeNaissance
     * @param adressePostale
     * @param email
     * @param profil
     * @param dateCreation
     * @param dateMaj
     */
    public Annuaire(String nom, String prenom, Date dateDeNaissance, String adressePostale, String email, Profil profil, String dateCreation,Date dateMaj) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateDeNaissance = dateDeNaissance;
        this.adressePostale = adressePostale;
        this.email = email;
        this.profil = profil;
        this.dateMaj = dateMaj;
        this.dateCreation = dateCreation;
    }

    //    Getters and Setteurs

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public String getAdressePostale() {
        return adressePostale;
    }

    public void setAdressePostale(String adressePostale) {
        this.adressePostale = adressePostale;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }


    public String getDateCreation() {
        return dateCreation;
    }

    public String toString(Annuaire annuaire, Boolean ajout){

        if(ajout){
        return annuaire.getNom() + ";" + annuaire.getPrenom() + ";" + annuaire.getEmail() + ";" + annuaire.getAdressePostale()  + ";" + annuaire.getDateDeNaissance() + ";"
                + annuaire.getProfil() + ";"
                + annuaire.getDateAjout() + ";" + annuaire.getDateMaj().toString();}

        else{
        return annuaire.getNom() + ";" + annuaire.getPrenom() + ";" + annuaire.getEmail() + ";" + annuaire.getAdressePostale()  + ";" + annuaire.getDateDeNaissance() + ";"
                + annuaire.getProfil() + ";"
                + annuaire.getDateCreation() + ";" + annuaire.getDateMaj().toString();}
    }
}


