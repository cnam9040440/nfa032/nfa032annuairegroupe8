package travaildegroupe.annuairegroupe8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Authentification {



    private String mail;
    private String mdp;
    private Role role;

    public String getMail() {
        return mail;
    }

    public String getMdp() {
        return mdp;
    }

    /**
     * setter pour l'objet Role
     * @param role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * getter pour l'objet Role
     * @return Role
     */
    public Role getRole() {
        return role;
    }


    /**
     * méthode vérifiant l'identité d'une personne
     * @param fichierComptes
     * @return
     */
    public Role login(File fichierComptes){
        CollecterIdentifiant();
        tentativeDeLog(fichierComptes);
        if (getRole() != null){
            return getRole();
        }
        return null;
    }

    /**
     * méthodes vérifiant les identifiants d'un individu et récupérant le role du dit individu
     * @param fichierComptes
     */
    public void tentativeDeLog(File fichierComptes) {


        BufferedReader lecteur = null;
        String ligne = "";

        try {
            lecteur = new BufferedReader(new FileReader(fichierComptes.getAbsoluteFile()));
            while ((ligne = lecteur.readLine()) != null ) {

                if (ligne.split(";")[0].equals(getMail()) && ligne.split(";")[1].equals(getMdp())) {
                    String stringRole = ligne.split(";")[2];
                    setRole(Role.valueOf(stringRole));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                lecteur.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * méthode permettant de récupérer les identifiants de l'individu souhaitant s'authentifier
     */
    public void CollecterIdentifiant() {

        Scanner userInput = new Scanner(System.in);
        System.out.println("Entrez vos identifiants");
        System.out.print("Votre adresse mail :");
        try {
            mail = userInput.nextLine();
        } catch (Exception mailErreur) {
            System.out.println("mail non reconnu");
        }
        System.out.print("\nVotre mot de passe :");
        try {
            mdp = userInput.nextLine();
        } catch (Exception mdpErreur) {
            System.out.println("mot de passe non reconnu");
        }
    }

}
